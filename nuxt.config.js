export default {
  target: "static",

  head() {
    return {
      htmlAttrs: {
        lang: "en"
      },
      meta: [
        { charset: "utf-8" },
        { name: "viewport", content: "width=device-width, initial-scale=1" },
        { hid: "description", name: "description", content: "" },
        { name: "format-detection", content: "telephone=no" }
      ],
      link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
    };
  },

  css: ["~/assets/styles/global.scss"],

  publicRuntimeConfig: {
    QUIZ_URL: process.env.QUIZ_URL
  },

  plugins: [
    { src: "~/plugins/axios" },
    { src: "~/plugins/v-tooltip.client" },
    { src: "~/plugins/vue-js-toggle-button.client" },
    { src: "~/plugins/vue-smooth-scroll.client" },
    { src: "~/plugins/vue-smooth-picker.client" },
    { src: "~/plugins/v-check-view.client" },
    { src: "~/plugins/vue-notification.client" },
    { src: "~/plugins/vue-notification.server" },
    { src: "~/plugins/font-awesome.client" },
    { src: "~/plugins/v-click-outside" }
  ],

  components: true,

  buildModules: [
    "@nuxt/typescript-build",
    "@nuxtjs/stylelint-module",
    "@nuxtjs/style-resources",
    "@nuxtjs/composition-api/module",
    ["@pinia/nuxt", { disableVuex: true }]
  ],

  modules: [
    "@nuxtjs/axios",
    "@nuxtjs/i18n",
    "@nuxtjs/axios",
    "@nuxtjs/robots",
    "@nuxtjs/sentry"
  ],

  styleResources: {
    scss: ["~/assets/styles/core/variables.scss"]
  },

  robots: {
    UserAgent: "*",
    Disallow: "/"
  },

  i18n: {
    locales: [{ code: "en", file: "en/en.js", dir: "ltr" }],
    defaultLocale: "en",
    vueI18n: {
      fallbackLocale: "en"
    },
    lazy: true,
    langDir: "locales/"
  },

  sentry: {
    dsn: process.env.SENRY_DSN,
    tracesSampleRate: 0.5,
    browserTracing: {},
    vueOptions: {
      trackComponents: true
    },
    debug: true,
    config: {
      tracesSampleRate: 0.5,
      sampleRate: 1,
      debug: true,
      beforeSend(event, hint) {
        if (
          hint &&
          hint.originalException &&
          hint.originalException.isAxiosError
        ) {
          if (
            hint.originalException.response &&
            hint.originalException.response.data
          ) {
            const contexts = {
              ...event.contexts
            };
            contexts.errorResponse = {
              data: hint.originalException.response.data
            };
            event.contexts = contexts;
          }
        }

        return event;
      },
      environment: process.env.DEV_MODE === "true" ? "dev" : "prod"
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    postcss: {
      postcssOptions: {
        plugins: {
          tailwindcss: {},
          autoprefixer: {}
        }
      }
    }
  }
};
