import { Screen } from "~/models/screen";

export default [
  {
    name: "SingleAnswerQuestion",
    type: "question",
    title: "",
    text: "",
    titlePlaceholder: "builder.yourQuestion",
    textPlaceholder: "builder.additionalDescription",
    answers: [
      {
        title: "",
        placeholder: "builder.answer",
        type: "button"
      },
      {
        title: "",
        placeholder: "builder.answer",
        type: "button"
      },
      {
        title: "",
        placeholder: "builder.answer",
        type: "button"
      }
    ],
    settings: [
      [
        {
          title: "builder.singleAnswerQuestionSettingNextOnClick",
          name: "nextOnClick",
          type: "toggle",
          value: false
        }
      ]
    ]
  },
  {
    name: "MultipleAnswerQuestion",
    type: "question",
    title: "",
    text: "",
    titlePlaceholder: "builder.yourQuestion",
    textPlaceholder: "builder.additionalDescription",
    answers: [
      {
        title: "",
        placeholder: "builder.answer",
        type: "button-multiple"
      },
      {
        title: "",
        placeholder: "builder.answer",
        type: "button-multiple"
      },
      {
        title: "",
        placeholder: "builder.answer",
        type: "button-multiple"
      }
    ],
    settings: []
  },
  {
    name: "TextQuestion",
    type: "question",
    title: "",
    text: "",
    titlePlaceholder: "builder.yourQuestion",
    textPlaceholder: "builder.additionalDescription",
    answers: [
      {
        value: "",
        type: "textfield",
        placeholder: "builder.addScreen.text"
      }
    ],
    settings: [
      [
        {
          title: "builder.textScreenSettingTextarea",
          name: "isTextarea",
          type: "toggle",
          value: false
        }
      ]
    ]
  },
  {
    name: "DateQuestion",
    type: "question",
    title: "",
    text: "",
    titlePlaceholder: "builder.yourQuestion",
    textPlaceholder: "builder.additionalDescription",
    answers: [
      {
        value: "",
        type: "date",
        placeholder: "builder.dateScreenChooseDate"
      }
    ],
    settings: [
      [
        {
          title: "builder.dateScreenSettingAddTimeInput",
          name: "includeTime",
          type: "toggle",
          value: false
        }
      ]
    ]
  },
  {
    name: "SelectQuestion",
    type: "question",
    title: "",
    text: "",
    titlePlaceholder: "builder.yourQuestion",
    textPlaceholder: "builder.additionalDescription",
    answers: [
      {
        value: [],
        type: "select",
        placeholder: "builder.chooseFromList"
      }
    ],
    settings: []
  },
  {
    name: "Loader",
    type: "loader",
    title: "",
    text: "",
    titlePlaceholder: "builder.yourQuestion",
    textPlaceholder: "builder.additionalDescription",
    answers: [],
    settings: [
      [
        {
          title: "builder.loader.settings.showNext",
          name: "noTimer",
          value: true,
          type: "checkbox",
          hint: ""
        },
        {
          title: "builder.loader.settings.timerRedirect",
          name: "withTimer",
          value: false,
          inputValue: 3,
          type: "checkbox",
          hint: "builder.loader.settings.timerRedirectHint"
        }
      ],
      [
        {
          title: "builder.loader.settings.imageLarge",
          name: "image-large",
          value: false,
          type: "checkbox",
          hint: ""
        },
        {
          title: "builder.loader.settings.imageMedium",
          name: "image-medium",
          value: true,
          type: "checkbox",
          hint: ""
        },
        {
          title: "builder.loader.settings.imageSmall",
          name: "image-small",
          value: false,
          type: "checkbox",
          hint: ""
        }
      ]
    ]
  }
] as Screen[];
