/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: "jit",
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}"
  ],
  theme: {
    animation: {
      glow: "2s ease-in-out infinite shadow-animated"
    },
    fontFamily: {
      sans: ["Inter", "ui-sans-serif", "system-ui", "sans-serif"]
    },
    colors: {
      white: "hsl(0, 0%, 100%)",
      "text-primary": "hsl(240, 7%, 35%)",
      "text-secondary": "hsl(232, 10%, 31%)",
      primary: "hsl(171, 100%, 41%)",
      "primary-2": "hsl(171, 100%, 41%, 0.5)",
      "primary-3": "hsl(171, 100%, 41%, 0.15)",
      "white-smoke": "hsl(0, 0%, 96%)",
      "silver-chalice": "hsl(233, 6%, 63%)",
      "gray-light": "hsl(0, 0%, 86%, 0.6)",
      "ghost-gray": "hsl(220, 11%, 73%)",
      cultured: "hsl(210, 7%, 98%)",
      gainsboro: "hsl(210, 6%, 93%)"
    }
  },
  plugins: []
};
