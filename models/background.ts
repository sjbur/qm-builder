export type Background = {
  preview: string;
  url: string;
};
