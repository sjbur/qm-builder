export type Background = {
  title: string;
  variants: string[];
};

export type Button = {
  border: string;
  borderActive: string;
};

export type Checkbox = {
  border: string;
  borderActive: string;
  background: string;
  backgroundActive: string;
};

export type DateTimePicker = {
  doneBackground: string;
  doneText: string;
};

export type Radio = {
  radioBorder: string;
  radioBackground: string;
  radioBorderActive: string;
  radioBackgroundActive: string;
};

export type Select = {
  borderActive: string;
};

export type Subscribe = {
  background: string;
  text: string;
};
export type Theme = {
  id: number;
  subscribe: Subscribe;
  button: Button;
  radio: Radio;
  select: Select;
  dateTimePicker: DateTimePicker;
  checkbox: Checkbox;
  background: Background[];
};
