import { Answer } from "~/models/answer";

export type Screen = {
  name?: string;
  type?: string;
  title?: string;
  text?: string;
  titlePlaceholder?: string;
  textPlaceholder?: string;
  answers?: Answer[];
  settings: any[];
};
