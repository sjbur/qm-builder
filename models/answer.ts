export type Answer = {
  title?: string;
  text?: string;
  placeholder?: string;
  value?: string;
  type?: string;
};
