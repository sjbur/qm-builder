module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  extends: [
    "@nuxtjs/eslint-config-typescript",
    "plugin:nuxt/recommended",
    "prettier"
  ],
  plugins: [],
  // add your custom rules here
  rules: {
    "vue/no-v-html": "off",
    "vue/component-name-in-template-casing": [
      "error",
      "kebab-case",
      {
        registeredComponentsOnly: true,
        ignores: []
      }
    ],
    "vue/multi-word-component-names": "off",
    "@typescript-eslint/no-unused-vars": "off",
    "max-lines": ["error", { max: 300, skipBlankLines: true }]
  }
};
