export function findSettingValue(settings, settingName) {
  let result = "";
  settings?.forEach((groupSetting) => {
    groupSetting.forEach((setting) => {
      if (setting.name === settingName) {
        result = setting.value;
      }
    });
  });
  return result;
}

export function findSettingInputValue(settings, settingName) {
  let result = "";
  settings?.forEach((groupSetting) => {
    groupSetting.forEach((setting) => {
      if (setting.name === settingName) {
        result = setting.inputValue;
      }
    });
  });
  return result;
}
