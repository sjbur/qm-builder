export default function ({ $axios, error: nuxtError }) {
  $axios.onError((error) => {
    if (process.browser) {
      $nuxt.$notify({
        title: "Error",
        text: error.response?.data?.message || error,
        type: "error",
        group: "foo"
      });
      return;
    }

    nuxtError({
      statusCode: error.response.status,
      message: error.message
    });
    return Promise.resolve(false);
  });
}
