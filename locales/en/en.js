import builder from "./builder.json";
import common from "./common.json";
import preview from "./preview.json";

export default {
  builder,
  common,
  preview
};
