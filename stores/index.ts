import { defineStore } from "pinia";
import Vue from "vue";
import themes from "~/data/themes.json";
import backgrounds from "~/data/backgrounds.json";
import { Screen } from "~/models/screen";
import { Theme } from "~/models/theme";

export const useQuizStore = defineStore("quiz", {
  state: () => ({
    screens: [] as Screen[],
    systemScreens: [] as Screen[],
    theme: undefined as Theme | undefined,
    themeDark: false,
    themeBackgroundStyle: "color",
    themeBackgroundValue: "",
    themes,
    mobileDeviceAccepted: false,
    previewCurrentScreen: 0
  }),
  actions: {
    init() {
      const localStorageData = localStorage.getItem("quiz_maker_state");

      if (localStorageData) {
        const {
          screens,
          systemScreens,
          theme,
          themeDark,
          themeBackgroundStyle,
          themeBackgroundValue,
          mobileDeviceAccepted
        } = JSON.parse(localStorageData);

        this.screens = screens;
        this.systemScreens = systemScreens;
        this.theme = theme;
        this.themeDark = themeDark;
        this.themeBackgroundStyle = themeBackgroundStyle;
        this.themeBackgroundValue = themeBackgroundValue;
        this.mobileDeviceAccepted = mobileDeviceAccepted;

        return;
      }

      this.theme = themes.light[0];
      this.themeBackgroundValue = themes.light[0].background[0].variants[0];
    },

    addScreen({
      screen,
      screenIndex = 0,
      systemScreen = false
    }: {
      screen: Screen;
      screenIndex: number;
      systemScreen: boolean;
    }) {
      if (!systemScreen) {
        this.screens.splice(screenIndex + 1, 0, screen);
        return;
      }

      this.systemScreens.splice(screenIndex + 1, 0, screen);
    },

    setScreen({
      index = 0,
      screen,
      systemScreen = false
    }: {
      index: number;
      screen: Screen;
      systemScreen: boolean;
    }) {
      if (!systemScreen) {
        Vue.set(this.screens, index, screen);
        this.saveState();
        return;
      }

      this.systemScreens[index] = screen;
      this.saveState();
    },

    copyScreen({
      screenIndex = 0,
      systemScreen = false
    }: {
      screenIndex: number;
      systemScreen: boolean;
    }) {
      if (!systemScreen) {
        this.screens.splice(
          screenIndex,
          0,
          JSON.parse(JSON.stringify(this.screens[screenIndex]))
        );
        this.saveState();
        return;
      }

      this.systemScreens.splice(
        screenIndex,
        0,
        JSON.parse(JSON.stringify(this.systemScreens[screenIndex]))
      );
      this.saveState();
    },

    removeScreen({
      index = 0,
      systemScreen = false
    }: {
      index: number;
      systemScreen: boolean;
    }) {
      if (!systemScreen) {
        this.screens.splice(index, 1);
        this.saveState();
        return;
      }

      this.systemScreens.splice(index, 1);
      this.saveState();
    },

    setScreenSetting({
      screenIndex,
      groupIndex,
      settingIndex,
      value,
      systemScreen = false
    }: {
      screenIndex: number;
      groupIndex: number;
      settingIndex: number;
      value: any;
      systemScreen: boolean;
    }) {
      if (!systemScreen) {
        Vue.set(
          this.screens[screenIndex].settings[groupIndex],
          settingIndex,
          value
        );

        this.saveState();
        return;
      }

      Vue.set(
        this.systemScreens[screenIndex].settings[groupIndex],
        settingIndex,
        value
      );
      this.saveState();
    },

    // Добавление вопросов, редактирование заголовков, добавление сущностей ответов и их удаление

    addQuestion({
      screenIndex = 0,
      screen,
      systemScreen = false
    }: {
      screenIndex: number;
      screen: Screen;
      systemScreen: boolean;
    }) {
      if (!systemScreen) {
        this.screens.splice(screenIndex + 1, 0, screen);
        this.saveState();
        return;
      }

      this.systemScreens.splice(screenIndex + 1, 0, screen);
      this.saveState();
    },

    setQuestionTitle({ screenIndex = 0, title = "", systemScreen = false }) {
      if (!systemScreen) {
        this.screens[screenIndex].title = title;
        this.saveState();
        return;
      }

      this.systemScreens[screenIndex].title = title;
      this.saveState();
    },

    addControl({
      control,
      screenIndex = 0,
      systemScreen = false
    }: {
      control: any;
      screenIndex: number;
      systemScreen: boolean;
    }) {
      if (!systemScreen) {
        this.screens[screenIndex].answers?.push(control);
        this.saveState();
        return;
      }

      this.systemScreens[screenIndex].answers?.push(control);
      this.saveState();
    },

    setControl({
      screenIndex = 0,
      answerIndex = 0,
      value,
      systemScreen = false
    }: {
      screenIndex: number;
      answerIndex: number;
      value: any;
      systemScreen: boolean;
    }) {
      if (!systemScreen) {
        const targetScreen = this.screens[screenIndex];
        const targetAnswers = targetScreen.answers;

        if (targetAnswers) {
          Vue.set(targetAnswers, answerIndex, value);
        }

        this.saveState();
        return;
      }

      const targetScreen = this.systemScreens[screenIndex];
      const targetAnswers = targetScreen.answers;

      if (targetAnswers) Vue.set(targetAnswers, answerIndex, value);

      this.saveState();
    },

    removeControl({
      screenIndex,
      controlIndex,
      systemScreen = false
    }: {
      screenIndex: number;
      controlIndex: number;
      systemScreen: boolean;
    }) {
      if (!systemScreen) {
        const targetScreen = this.screens[screenIndex];
        const targetAnswers = targetScreen.answers;

        if (targetAnswers) targetAnswers.splice(controlIndex, 1);

        this.saveState();
        return;
      }

      const targetScreen = this.systemScreens[screenIndex];
      const targetAnswers = targetScreen.answers;

      if (targetAnswers) targetAnswers.splice(controlIndex, 1);

      this.saveState();
    },

    setTheme(theme: Theme) {
      this.theme = theme;
      this.saveState();
    },

    setThemeDark(dark: boolean) {
      this.themeDark = dark;
      this.saveState();
    },

    setThemeBackground(style: any) {
      this.themeBackgroundStyle = style;
      this.saveState();
    },

    setThemeBackgroundValue(value: any) {
      this.themeBackgroundValue = value;
      this.saveState();
    },

    setPreviewCurrentScreen(screenIndex: number) {
      this.previewCurrentScreen = screenIndex;
    },

    saveState() {
      localStorage.setItem("quiz_maker_state", JSON.stringify(this.$state));
    },

    acceptMobile() {
      this.mobileDeviceAccepted = true;
      this.saveState();
    }
  },
  getters: {
    abstractBackgrounds: (state) =>
      state.themeDark ? backgrounds.dark.abstract : backgrounds.light.abstract,
    imageBackgrounds: (state) =>
      state.themeDark ? backgrounds.dark.image : backgrounds.light.image,
    screensValidationError: (state) => {
      let errorText = "";

      if (!state.screens.length) return "noScreens";

      state.screens.forEach((scr) => {
        if (scr?.title === "") {
          if (!scr.text) {
            errorText = "titleAndText";
          }
        }

        scr?.answers?.forEach((answer) => {
          if (answer.title === "") {
            errorText = "fillAnswers";
          }
        });
      });

      state.systemScreens.forEach((scr) => {
        if (scr?.title === "") {
          if (!scr.text) {
            errorText = "titleAndText";
          }
        }

        scr?.answers?.forEach((answer) => {
          if (answer.title === "") {
            errorText = "fillAnswers";
          }
        });
      });

      return errorText;
    }
  }
});
